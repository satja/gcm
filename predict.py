# Calculates the CF prediction errors.
# Usage: python3 predict.py [UPCC? 0/1]  [IPCC? 0/1] [MF? 0/1]

import pickle
from multiprocessing import Pool
import sys
from math import sqrt, inf, log2
from calculate import avg_scores, get_diff, sim_u, all_sets
from mf import matrix_factorization
import numpy as np
from time import time


def pcc(scores_list, other_list, n, m, query_list):
    assert len(scores_list) == n
    assert len(query_list) == n
    assert len(other_list) == m
    prediction_list = [[] for i in range(n)]
    lo, hi, _ = get_diff(scores_list)
    avgs = avg_scores(scores_list, (lo + hi) / 2)
    t0, t1 = 0, 0

    for i in range(n):
        #if i > 0 and i % 2000 == 0:  # Track progress
        #    print('pcc {:d}/{:d}, {:.2f} {:.2f}'.\
        #            format(i, n, t0, t1), file=sys.stderr)
        t0 -= time()
        sim_i = np.maximum(0, sim_u(i, scores_list, other_list, n, m, avgs))
        t0 += time()
        t1 -= time()
        for y, _ in query_list[i]:
            j, rj = other_list[y]
            if len(j) == 0:
                prediction = avgs[i]
            else:
                simsum = np.sum(sim_i[j])
                if simsum == 0:
                    prediction = avgs[i]
                else:
                    prediction = avgs[i] + np.sum(sim_i[j] * (rj - avgs[j])) / simsum
            prediction_list[i].append((y, prediction))
        t1 += time()

    return prediction_list

def errors(query_list, prediction_list):
    n = len(query_list)
    assert n == len(prediction_list)
    err1, err2 = 0, 0
    num_predictions = 0
    lo, hi = inf, -inf
    for i in range(n):
        Q = len(query_list[i])
        assert Q == len(prediction_list[i])
        num_predictions += Q
        for q in range(Q):
            err = prediction_list[i][q][1] - query_list[i][q][1]
            err1 += abs(err)
            err2 += err * err
            hi = max(hi, query_list[i][q][1])
            lo = min(lo, query_list[i][q][1])
    diff = hi - lo
    #print('diff={:.1f}, mae={:.1f}, num_predictions={:d}'.\
    #        format(diff, err1/num_predictions, num_predictions),
    #        file=sys.stderr)
    return (err1 / num_predictions / diff,  # NMAE
            sqrt(err2 / num_predictions) / diff,  # NRMSE
            topN(query_list, prediction_list, lo, diff),  # NDCG-5
            topN(query_list, prediction_list, lo, diff, 10),  # NDCG-10
            )

def topN(query_list, prediction_list, lo, diff, p=5):
    n = len(query_list)
    assert n == len(prediction_list)
    result = 0
    cnt = 0
    for i in range(n):
        k = min(len(query_list[i]), p)
        if k == 0: continue
        sorted_actual = sorted(query_list[i], key=lambda x: -x[1])
        sorted_my = sorted(prediction_list[i], key=lambda x: -x[1])
        rel = dict(query_list[i])
        dcg, idcg = 0, 0
        for i in range(1, k + 1):
            denom = log2(i + 1)
            item = sorted_my[i - 1][0]
            dcg += (2 ** ((rel[item] - lo) / diff) - 1) / denom
            idcg += (2 ** ((sorted_actual[i - 1][1] - lo) / diff) - 1) / denom
        if idcg > 0:
            cnt += 1
            result += dcg / idcg
    return result / cnt

def best_pars(dataset):
    # Features, Learning rate, Regularization factor
    # K, alpha, beta
    if dataset == 'Amazon QoS':
        return (21,0.09488204957050751,0.062048699629008816)
    if dataset == 'FilmTrust':
        return (5,0.030736630653625904,0.06478786646616244)
    if dataset == 'CiaoDVD':
        return (5,0.19124074089242094,0.1784196463662966)
    if dataset == 'MovieLens 100K':
        return (5,0.025141019640705742,0.11401352713149501)
    if dataset == 'Epinions':
        return (5,0.07285404383330711,0.1267224465512813)
    if dataset == 'MovieLens 1M':
        return (20,0.008621709750005549,0.01813993632844733)
    if dataset == 'Jester':
        return (15, 0.044, 0.090)
    if dataset == 'Flixter':
        return (24, 0.011, 0.023)
    if dataset == 'Netflix':
        return (9, 0.004, 0.063)
    if dataset == 'MovieTweetings':
        return (5, .067, .013)
    if dataset == 'BookCrossing':
        return (13, 0.043, 0.052)
    if dataset == 'SongsDataset':
        return (5, 0.06542949517105365, 0.09533273768186418)
    if dataset == 'Anime':
        return (11, 0.009, 0.006)
    if dataset == 'Yahoo3':
        return (13, 0.092, 0.072)
    if dataset == 'Yahoo4':
        return (5, 0.040, 0.084)

def predict(dataset):
    with open('pickle/' + dataset, 'rb') as f:
        train_user_list = pickle.load(f)
        train_item_list = pickle.load(f)
        test_user_list = pickle.load(f)
        test_item_list = pickle.load(f)
    n = len(train_user_list)
    m = len(train_item_list)
    assert n == len(test_user_list)
    assert m == len(test_item_list)

    if sys.argv[1] == '1':
        print(dataset, 'UPCC', sep=', ', end=', ')
        prediction_list = pcc(train_user_list, train_item_list, n, m, test_user_list)
        print("NMAE {:.3f}, NRMSE {:.3f}, nDCG-5 {:.3f}, nDCG-10 {:.3f}".format(
            *errors(test_user_list, prediction_list)))

    if sys.argv[2] == '1':
        print(dataset, 'IPCC', sep=', ', end=', ')
        prediction_list = pcc(train_item_list, train_user_list, m, n, test_item_list)
        print("NMAE {:.3f}, NRMSE {:.3f}, nDCG-5 {:.3f}, nDCG-10 {:.3f}".format(
            *errors(test_item_list, prediction_list)))

    if sys.argv[3] == '1':
        print(dataset, 'MF', sep=', ', end=', ')
        features, learning_rate, reg_factor = best_pars(dataset)
        prediction_list = matrix_factorization(
                train_user_list, n, m, test_user_list,
                features, learning_rate, reg_factor)
        print("NMAE {:.3f}, NRMSE {:.3f}, nDCG-5 {:.3f}, nDCG-10 {:.3f}".format(
            *errors(test_user_list, prediction_list)))
    sys.stdout.flush()

if __name__ == '__main__':
    with Pool(processes=4) as p:
        p.map(predict, all_sets)
