import sys
import os
import pickle
from scipy.stats import pearsonr
import pandas as pd
from numpy import std, sign
import matplotlib.pyplot as plt
import matplotlib
from calculate import all_sets
from math import log

def parse_results(results):
    with open('results.csv', 'r') as f:
        for line in f:
            l = line.strip().split(', ')
            dataset = l[0]
            if dataset in all_sets:
                alg = l[1]
                if alg in ['GUCM', 'GICM']:
                    results[(dataset, alg)] = float(l[2])
                    common = float(l[3])
                    overall = float(l[4])
                    v = float(l[5])
                    sr = float(l[6])
                    sp = float(l[7])
                    results[(dataset, alg + 'simDeg')] = common
                    results[(dataset, alg + 'simPairs')] = sp
                    continue
                results[(dataset, alg + ' nDCG')] = float(l[5])
                results[(dataset, alg + ' nMAE')] = float(l[2])
                results[(dataset, alg + ' nRMSE')] = float(l[3])
    for dataset in all_sets:
        results[(dataset, 'GCM')] = (results[(dataset, 'GUCM')] + results[(dataset, 'GICM')]) / 2
        results[(dataset, 'simPairs')] = (results[(dataset, 'GUCMsimPairs')] + results[(dataset, 'GICMsimPairs')]) / 2
        results[(dataset, 'simDeg')] = (results[(dataset, 'GUCMsimDeg')] + results[(dataset, 'GICMsimDeg')]) / 2

def table_basic(datasets, results):
    f = open('table.tex', 'w')

    f.write("dataset")
    for e in ['GUCM', 'GICM', 'sim. user pairs', 'sim. item pairs',
            'user sim. deg.', 'item sim. deg.', 'UPCC nDCG', 'IPCC nDCG', 'MF nDCG']:
        f.write(' & ' + e)
    f.write('\\\\ \hline\n')

    for d in datasets:
        f.write(d.replace(' ', '').replace('4', 'Movies'))
        for e in ['GUCM', 'GICM', 'GUCMsimPairs', 'GICMsimPairs', 'GUCMsimDeg',
                'GICMsimDeg', 'UPCC nDCG', 'IPCC nDCG', 'MF nDCG']:
            f.write(' & {:.3f} '.format(results[(d, e)]))
        f.write('\\\\ \hline\n')
    f.close()
    os.system('cp table.tex ~/Dropbox/gcm_clanak/figures_and_tables/')

def corr(coef, keyA, keyB, datasets, f):
    a, b = [], []
    for dataset in datasets:
        a.append(results[(dataset, keyA)])
        b.append(results[(dataset, keyB)])
    pe0, pe1 = coef(a, b)
    f.write("%s & %s & %.3f & %.3f \\\\ \\hline\n" % (
            keyA.replace('GCM', '(GUCM+GICM)/2'), keyB, pe0, pe1))
    return pe0, pe1

def correlate(datasets, results):
    st = {pearsonr: 'Pearson'}  #, spearmanr: 'Spearman', kendalltau: 'Kendall'}
    f = open('corr_table.tex', 'w')

    ''' Correlation of GUCM and GICM:
    '''
    for coef in [pearsonr]:
        pe0, pe1 = corr(coef, 'GUCM', 'GICM', datasets, f)
        print(st[coef], 'GUCM', 'GICM', end=' ')
        print("({:.3f}, {:.3f})".format(pe0, pe1))
    print()

    ''' Correlation with predictions:
    '''
    for measure, alg in zip(['GUCM', 'GICM', 'GCM'], ['UPCC', 'IPCC', 'MF']):
            for err in ['nDCG']:
                for coef in [pearsonr]:
                    pe0, pe1 = corr(coef, measure, alg + ' ' + err, datasets, f)
                    print(st[coef], measure, alg + ' ' + err, end=' ')
                    print("({:.3f}, {:.3f})".format(pe0, pe1))
            print()
    print()

    ''' Correlation of existing measures with predictions:
    '''
    for measure, alg in zip(['GUCMsimPairs', 'GICMsimPairs', 'simPairs'],
            ['UPCC', 'IPCC', 'MF']):
            for err in ['nDCG']:
                for coef in [pearsonr]:
                    pe0, pe1 = corr(coef, measure, alg + ' ' + err, datasets, f)
                    print(st[coef], measure, alg + ' ' + err, end=' ')
                    print("({:.3f}, {:.3f})".format(pe0, pe1))
            print()
    print()

    ''' Correlation of existing measures with predictions:
    '''
    for measure, alg in zip(['GUCMsimDeg', 'GICMsimDeg', 'simDeg'],
            ['UPCC', 'IPCC', 'MF']):
            for err in ['nDCG']:
                for coef in [pearsonr]:
                    pe0, pe1 = corr(coef, measure, alg + ' ' + err, datasets, f)
                    print(st[coef], measure, alg + ' ' + err, end=' ')
                    print("({:.3f}, {:.3f})".format(pe0, pe1))
            print()
    print()

    ''' Correlation with avg. similarities:
    '''
    for m1 in ['GUCM', 'GICM']:
        for m2 in ['simDeg', 'simPairs']:
            for coef in [pearsonr]:
                pe0, pe1 = corr(coef, m1, m1 + m2, datasets, f)
                print(st[coef], m1, m2, end=' ')
                print("({:.3f}, {:.3f})".format(pe0, pe1))
    f.close()
    os.system('cp corr_table.tex ~/Dropbox/gcm_clanak/figures_and_tables/')

def plot_similarities(datasets, results):
    for measure in ['GICM', 'GUCM']:
        datasets.sort(key=lambda x: results[(x, measure)])
        sim1 = measure + 'simPairs'
        sim2 = measure + 'simDeg'
        raw_data = {'name': [], measure: [], sim1: [], sim2: [], }
        for name in datasets:
            raw_data['name'].append(
                    name.replace(' 100K', '100K').replace(' 1M', '1M ').replace('4', 'Movies')
                    )
            raw_data[measure].append(results[(name, measure)])
            x = results[(name, sim1)]
            y = results[(name, sim2)]
            raw_data[sim1].append(x)
            raw_data[sim2].append(y)
        df = pd.DataFrame(raw_data, columns = ['name', measure, sim1, sim2])
        matplotlib.rcParams.update({'font.size': 26})
        pos = list(range(len(df[measure])))
        width = 0.2
        fig, ax = plt.subplots(figsize=(30, 10))
        gcm = plt.plot(pos, df[measure], 'ko-',)
        sim1 = plt.plot(pos, df[sim1], 'ro-')
        sim2 = plt.plot(pos, df[sim2], 'bo-')
        plt.xlim(min(pos)-width, max(pos)+4*width)
        plt.ylim([0, 1])
        plt.grid()
        plt.show()
        plt.savefig('figures/bar' + measure + '.pdf', format='pdf', bbox_inches='tight')
        plt.close()

def plot_predictions(datasets, results):
    for measure, alg in zip(['GCM', 'GICM', 'GUCM'], ['MF', 'IPCC', 'UPCC']):
        datasets.sort(key=lambda x: results[(x, measure)])
        print(max(results[(d, measure)] for d in datasets[:4]))
        print(min(results[(d, measure)] for d in datasets[4:]))
        err5 = alg + ' nDCG'
        raw_data = {'name': [], measure: [], err5: [], }
        for name in datasets:
            raw_data['name'].append(
                    name.replace(' 100K', '100K').replace(' 1M', '1M ').replace('4', 'Movies'))
            raw_data[measure].append(results[(name, measure)])
            x = results[(name, err5)]
            raw_data[err5].append(x)
        df = pd.DataFrame(raw_data, columns = ['name', measure, err5])
        matplotlib.rcParams.update({'font.size': 26})
        pos = list(range(len(df[measure])))
        width = 0.3
        fig, ax = plt.subplots(figsize=(30, 10))
        ax2 = ax.twinx()
        ax.bar(pos, df[measure], width, alpha=0.5, color='black',)
        ax2.bar([p + width for p in pos], df[err5], width, alpha=0.5, color='r',)
        ax2.axhline(y=.94, linestyle='--', linewidth=3, color='b')
        ax.axvline(x = pos[4] - .15, linestyle='--', linewidth=3, color='b')
        ax.set_xticks([p for p in pos])
        ax.set_xticklabels(df['name'], rotation=15)
        plt.xlim(min(pos)-width, max(pos)+3*width)
        ax2.set_ylim([0.86, 1])
        ax.set_ylabel(measure.replace('GCM', '(GUCM + GICM) / 2'), color='k', fontsize=30)
        ax2.set_ylabel(err5, fontsize=30, color='k')
        plt.grid()
        #plt.show()
        plt.savefig('figures/bar' + alg + '.pdf', format='pdf', bbox_inches='tight')
        plt.close()
    os.system('cp figures/bar*.pdf ~/Dropbox/gcm_clanak/figures_and_tables/')

def u_vs_i(datasets, results):
    for e in ['nMAE', 'nRMSE', 'nDCG']:
        good, tot = 0, 0
        for d in datasets:
            a = results[(d, 'GUCM')] - results[(d, 'GICM')]
            b = results[(d, 'UPCC ' + e)] - results[(d, 'IPCC ' + e)]
            if sign(a) == sign(b):
                good += 1
            tot += 1
        print('GUCM vs GICM,', e, good/tot, len(datasets))
    print()

def set_vs_set(datasets, results):
    for i in range(len(datasets)):
        for j in range(len(datasets)):
            a = datasets[i]
            b = datasets[j]
            if results[(a, 'GUCM')] < results[(b, 'GUCM')] and\
                    results[(a, 'GICM')] < results[(b, 'GICM')]:
                results[(a, b)] = True
    errs = ['GUCMsr', 'GICMsr',
            'UPCC nDCG', 'IPCC nDCG', 'MF nDCG',
            ]
    for e in errs:
        good, tot = 0, 0
        for i in range(len(datasets)):
            for j in range(i):
                a = datasets[i]
                b = datasets[j]
                if (a, b) in results:
                    tot += 1
                    if results[(a, e)] < results[(b, e)]:
                        good += 1
                if (b, a) in results:
                    tot += 1
                    if results[(b, e)] < results[(a, e)]:
                        good += 1
        ratio = good / tot
        if ratio >= 0.7:
            print('gcm compare predict', e, ratio,
                    tot, len(datasets) * (len(datasets) - 1) // 2)
    print()

if __name__ == '__main__':
    results = dict()
    parse_results(results)
    all_sets.sort(key=lambda x: results[(x, 'GCM')])
    #u_vs_i(all_sets, results)
    #set_vs_set(all_sets, results)
    correlate(all_sets, results)
    #table_basic(all_sets, results)
    #plot_predictions(all_sets, results)
