from calculate import *
from random import randint, seed

avgs = [randint(-3000, 3000) for i in range(50000)]
noise_matrix = [randint(-100, 100) for i in range(5000 * 5000)]
cnt = 0

def cluster_test_users(k, noise=False, exp=3):
    #print('k =', k)
    for tp in range(0, exp):
        num_users = 5 * 10**tp
        num_items = 5 * 10**tp
        num_users = num_items = 5000
        user_list = [([], []) for i in range(num_users)]
        item_list = [([], []) for j in range(num_items)]
        for i in range(num_users):
            for j in range(num_items):
                seed(j * 20000 + i % k)
                rating = randint(0, 10000)
                if noise:
                    rating += avgs[i]  #+ noise_matrix[i * num_items + j]
                user_list[i][0].append(j)
                user_list[i][1].append(rating)
                item_list[j][0].append(i)
                item_list[j][1].append(rating)
        print(measures(user_list, item_list, num_users, num_items))
    #print()

def cluster_test_items(k, noise=False, exp=3):
    #print('k =', k)
    for tp in range(0, exp):
        num_users = 5 * 10**tp
        num_items = 5 * 10**tp
        num_users = num_items = 5000
        user_list = [([], []) for i in range(num_users)]
        item_list = [([], []) for j in range(num_items)]
        for i in range(num_users):
            for j in range(num_items):
                seed(i * 20000 + j % k)
                rating = randint(0, 10000)
                if noise:
                    rating += avgs[j]  #+ noise_matrix[i * num_items + j]
                user_list[i][0].append(j)
                user_list[i][1].append(rating)
                item_list[j][0].append(i)
                item_list[j][1].append(rating)
        print(measures(user_list, item_list, num_users, num_items))
    #print()

for k in range(1, 10):
    cluster_test_users(k)
#    cluster_test_users(k, True)
for k in range(1, 11):
    cluster_test_users(k * 10)
#    cluster_test_users(k * 10, True)
