from calculate import *
from random import randint, seed

def random_test(tip, exp=3):
    for tp in range(0, exp):
        num_users = 5 * 10**tp
        num_items = 5 * 10**tp
        num_users = num_items = 5000
        user_list = [([], []) for i in range(num_users)]
        item_list = [([], []) for j in range(num_items)]
        for i in range(num_users):
            for j in range(num_items):
                rating = randint(0, tip)
                user_list[i][0].append(j)
                user_list[i][1].append(rating)
                item_list[j][0].append(i)
                item_list[j][1].append(rating)
        print(measures(user_list, item_list, num_users, num_items))

def regular_test(tip, exp=3):
    for tp in range(0, exp):
        num_users = 5 * 10**tp
        num_items = 5 * 10**tp
        user_list = [([], []) for i in range(num_users)]
        item_list = [([], []) for j in range(num_items)]
        for i in range(num_users):
            for j in range(num_items):
                seed(i * 20000 + j % 2)
                rating = randint(0, tip)
                user_list[i][0].append(j)
                user_list[i][1].append(rating)
                item_list[j][0].append(i)
                item_list[j][1].append(rating)
        print(measures(user_list, item_list, num_users, num_items))

for k in range(1, 6):
    random_test(2**k - 1)
#for k in range(5):
#    regular_test(2**k)
