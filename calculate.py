# Estimates the dataset quality: calculates GUCM, GICM and related measures.
# Usage: python3 calculate.py ["user"/"item"] 

from bisect import *
from multiprocessing import Pool
from operator import itemgetter
from scipy.stats import tstd
import sys
from math import inf, sqrt, tanh
import pickle
import numpy as np
from time import time

all_sets = [
    'FilmTrust',
    'Amazon QoS',
    'MovieLens 100K',
    'Netflix',
    'Epinions',
    'CiaoDVD',
    'MovieLens 1M',
    'Jester',
    'Flixter',
    'BookCrossing',
    'Yahoo4',
    'MovieTweetings', 
    'Anime',
    ]

def uad(scores, diff, treshold):
    treshold = max(treshold, diff * 0.001)
    k = len(scores)
    similar_pairs = 0
    scores.sort()
    last = 0
    for i in range(1, k):
        while scores[i] - scores[last] >= treshold:
            last += 1
        similar_pairs += i - last
    p = similar_pairs / (k * (k - 1) / 2)
    q = (2 - treshold / diff) * treshold / diff
    return max(0, p - q)

def avg_scores(scores_list, default=0):
    avg_scores = []
    for _, scores in scores_list:
        if len(scores) == 0:
            avg_scores.append(default)
            continue
        avg_scores.append(np.mean(scores))
    return np.array(avg_scores)

def get_diff(user_list):
    max_rating = -inf
    min_rating = inf
    for _, ratings in user_list:
        for rating in ratings:
            min_rating = min(min_rating, rating)
            max_rating = max(max_rating, rating)
    return min_rating, max_rating, max_rating - min_rating

def gcm(user_list, item_list, weighted=True):
    num_users = len(user_list)
    num_items = len(item_list)

    min_rating, _, diff = get_diff(user_list)
    user_avgs = avg_scores(user_list, min_rating + diff / 2)
    user_std_dev = tstd(user_avgs)

    item_scores = [[] for j in range(num_items)]
    for i in range(num_users):
        for j, rating in zip(user_list[i][0], user_list[i][1]):
            item_scores[j].append(rating - user_avgs[i])

    cnt = 0
    gucm = 0
    for j in range(num_items):
        k = len(item_scores[j])
        if k <= 1: continue
        if weighted:
            cnt += k
            gucm += k * uad(item_scores[j], diff, user_std_dev)
        else:
            cnt += 1
            gucm += uad(item_scores[j], diff, user_std_dev)
    return gucm / cnt

def sim_u(i, scores_list, other_list, n, m, avgs):
    sim_i = np.zeros(n)
    if len(scores_list[i][0]) > 1:
        denom_i = np.zeros(n)
        denom_j = np.zeros(n)
        for y, ri in zip(scores_list[i][0], scores_list[i][1]):
            j, rj = other_list[y]
            sim_i[j] += (ri - avgs[i]) * (rj - avgs[j])
            denom_i[j] += (ri - avgs[i]) ** 2
            denom_j[j] += (rj - avgs[j]) ** 2
        denom = np.sqrt(denom_i * denom_j)
        denom[denom == 0] = 1
        sim_i /= denom
    return sim_i

def avg_sim(user_list, item_list, dataset):
    num_users = len(user_list)
    num_items = len(item_list)
    min_rating, _, diff = get_diff(user_list)
    avgs = avg_scores(user_list, min_rating + diff / 2)

    BM = 70 * 1000
    if num_users <= BM:
        #print('using np bool matrix', file=sys.stderr)
        similar = np.zeros((num_users, num_users), dtype=np.bool)
    else:
        similar = [[] for u in range(num_users)]
    treshold = diff / 10
    for it, (users, ratings) in enumerate(item_list):
        vals = sorted(set(ratings))
        us = dict()
        for user, rating in zip(users, ratings):
            if rating in us:
                us[rating].append(user)
            else:
                us[rating] = [user]
        for i, v in enumerate(vals):
            us[v] = np.array(sorted(us[v]))
            for j, u1 in enumerate(us[v]):
                if num_users <= BM:
                    similar[u1][us[v][:j]] = 1
                    similar[us[v][:j], u1] = 1
                    continue
                for k in range(j):
                    similar[us[v][k]].append(u1)
        for i, v in enumerate(vals):
            for j in range(i + 1, len(vals)):
                if vals[j] >= v + treshold:
                    break
                if num_users <= BM:
                    similar[us[v][:, None], us[vals[j]]] = 1
                    similar[us[vals[j]][:, None], us[v]] = 1
                    continue
                for u1 in us[v]:
                    for u2 in us[vals[j]]:
                        if u1 < u2:
                            similar[u1].append(u2)
                        else:
                            similar[u2].append(u1)
        #if it % 10**3 == 0:  # Track progress:
        #    print(dataset, 'A', it, '/', num_items, time() - start, file=sys.stderr)
        if num_users > BM and it % 10**3 == 0:
            for u in range(num_users):
                similar[u] = list(set(similar[u]))

    overall_sim, common_sim, cnt = 0, 0, 0
    sim_pairs = 0
    for u in range(num_users):
        #if u % 10**3 == 0:  # Track progress:
        #    print(dataset, 'B', u, '/', num_users, time() - start, file=sys.stderr)
        sim_i = sim_u(u, user_list, item_list, num_users, num_items, avgs)
        overall_sim += np.sum(sim_i) - sim_i[u]
        if num_users <= BM:
            similar_users = [v for v in range(num_users) if similar[u][v] == 1]
        else:
            similar_users = set(similar[u])
        for v in similar_users:
            common_sim += sim_i[v]
            cnt += 1
        sim_pairs += np.sum(sim_i >= 0.5) - 1
    sim_pairs /= (num_users ** 2 - num_users)
    overall_sim /= (num_users ** 2 - num_users)
    common_sim /= cnt

    v = common_sim / max(overall_sim, 1e-9)
    ratings = sum(len(l) for l, _ in user_list)
    assert ratings == sum(len(l) for l, _ in item_list)
    sampling_rate = tanh(num_users * num_items / (max(v, 1e-9) * sqrt(num_users) * ratings))
    return common_sim, overall_sim, v, sampling_rate, sim_pairs

def calculate(dataset):
    with open('pickle/' + dataset, 'rb') as f:
        user_list = pickle.load(f)
        item_list = pickle.load(f)
    if sys.argv[1] == 'user':
        pars = (user_list, item_list)
    if sys.argv[1] == 'item':
        pars = (item_list, user_list)
    g = gcm(pars[0], pars[1])
    sd, _, _, _, sp = avg_sim(pars[0], pars[1], dataset)
    print("{}, G{}CM {:.3f}, similar_{}_pairs {:.3f}, {}_similarity_degree {:.3f}".format(
        dataset, sys.argv[1][0].upper(), g, sys.argv[1], sp, sys.argv[1], sd))
    sys.stdout.flush()

if __name__ == '__main__':
    with Pool(processes=4) as p:
        p.map(calculate, all_sets)
