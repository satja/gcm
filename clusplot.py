import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


raw_data = {'k': [],
        'GUCM': [],
        'GICM': [],
        }
with open('cluster_results.txt', 'r') as f:
    lines = f.readlines()
    for i, line in enumerate(lines):
        data = line.split(',')
        k = data[0].split()[0]
        raw_data['k'].append(int(k))
        raw_data['GUCM'].append(float(data[1]))
        raw_data['GICM'].append(float(data[2]))
df = pd.DataFrame(raw_data, columns = ['k', 'GUCM', 'GICM'])
print(df)

# Setting the positions and width for the bars
pos = list(range(len(df['GUCM'])))
width = 0.3

# Plotting the bars
fig, ax = plt.subplots(figsize=(20, 10))

# Create a bar with pre_score data,
# in position pos,
plt.bar(pos,
        #using df['pre_score'] data,
        df['GUCM'],
        # of width
        width,
        # with alpha 0.5
        alpha=0.5,
        # with color
        color='purple',
        # with label the first value in first_name
        #label=df['name'][0],
        )

# Create a bar with mid_score data,
# in position pos + some width buffer,
plt.bar([p + width for p in pos],
        #using df['mid_score'] data,
        df['GICM'],
        # of width
        width,
        # with alpha 0.5
        alpha=0.5,
        # with color
        color='r',
        # with label the second value in first_name
        #label=df['name'][1],
        )


ax.set_xlabel('K')

# Set the position of the x ticks
ax.set_xticks([p + width for p in pos])

# Set the labels for the x ticks
ax.set_xticklabels(df['k'])

# Setting the x-axis and y-axis limits
plt.xlim(min(pos)-width, max(pos)+width*5)
plt.ylim([0, 1])
#plt.ylim([0, max(df['GUCM'] + df['GICM'] + df['UPCC NMAE'] + df['IPCC NMAE'])] )

# Adding the legend and showing the plot
plt.legend(['GUCM', 'GICM'],
        loc='upper right')
plt.grid(which='minor')
ax.set_xlabel('Number of clusters of equal users')
plt.show()
