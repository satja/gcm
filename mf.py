import numpy as np
from multiprocessing import Pool
import sys
import pickle
from random import randrange, uniform
from math import inf, sqrt
from calculate import get_diff, all_sets

def matrix_factorization(scores, N, M, query_list, K, alpha, beta,
        validation=None, steps=100):
    min_rating, _, diff = get_diff(scores)

    np.seterr(all='raise')
    P = np.random.rand(N, K)
    Q = np.random.rand(M, K)
    for step in range(steps):
        #if query_list and (step + 1) % 20 == 0:  # Track progress
        #    print('mf {:d}/{:d}'.format(step, steps), file=sys.stderr)
        for i in range(N):
            for j, rating in zip(scores[i][0], scores[i][1]):
                try:
                    eij = (rating - min_rating) / diff - np.dot(P[i], Q[j])
                    P[i] += alpha * (eij * Q[j] - beta * P[i])
                    Q[j] += alpha * (eij * P[i] - beta * Q[j])
                except:
                    if query_list is not None:
                        print('OVERFLOW!')
                        print('OVERFLOW!', file=sys.stderr)
                        return query_list
                    return 777

    if validation is not None:
        err, err2 = 0, 0
        cnt = 0
        for i in range(N):
            cnt += len(validation[i])
            for j, r in validation[i]:
                rating = np.dot(P[i], Q[j]) * diff + min_rating
                err += abs(rating - r)
                err2 += (rating - r) ** 2
        #return err / cnt / diff  --> MAE
        return sqrt(err2 / cnt) / diff  # RMSE

    if query_list is not None:
        prediction_list = [[] for i in range(N)]
        for i in range(N):
            for j, _ in query_list[i]:
                rating = np.dot(P[i], Q[j]) * diff + min_rating
                prediction_list[i].append((j, rating))
        return prediction_list

def find_pars(dataset):
    sys.stderr.flush()
    with open('pickle/' + dataset, 'rb') as f:
        training_set = pickle.load(f)
        item_scores = pickle.load(f)
    n = len(training_set)
    m = len(item_scores)

    scores = [
            [([], []) for i in range(n)]
            for k in range(5)]
    validation = [
            [[] for i in range(n)]
            for k in range(5)]
    for i in range(n):
        j, ratings = training_set[i]
        q = len(j)
        assert len(ratings) == q
        for it in range(q):
            K = randrange(5)
            validation[K][i].append((j[it], ratings[it]))
            for k in range(5):
                if k != K:
                    scores[k][i][0].append(j[it])
                    scores[k][i][1].append(ratings[it])
        for k in range(5):
            scores[k][i] = (np.array(scores[k][i][0]), np.array(scores[k][i][1]))
    print('---------------------- start', dataset, '-------------------------', file=sys.stderr)

    min_err = inf
    k, alpha, beta = 10, 0.02, 0.02
    for steps in [10, 20, 40, 80]:
        for it in range(10):
            features = max(5, int(uniform(k / 2, k * 2) + .5))
            learning_rate = uniform(alpha / 2, alpha * 2)
            reg_factor = uniform(beta / 2, beta * 2)
            err = sum(matrix_factorization(
                        scores[k], n, m, None,
                        features, learning_rate, reg_factor,
                        validation[k], steps)
                        for k in range(5))
            print(dataset, steps, "best=(%d %.3lf %.3lf) - %.2lf, curr=(%d %.3lf %.3lf) - %.2lf"
                    % (k, alpha, beta, min_err, features, learning_rate, reg_factor, err),
                    file=sys.stderr)
            if err < min_err:
                min_err = err
                k, alpha, beta = features, learning_rate, reg_factor
    print(dataset, k, alpha, beta, min_err, sep=', ')
    sys.stdout.flush()

if __name__ == '__main__':
    with Pool(processes=4) as p:
        results = p.map(find_pars, all_sets)
