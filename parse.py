from multiprocessing import Pool
import sys
import pickle
import numpy as np
from bisect import *
from random import uniform, sample
from calculate import get_diff


def parse(dataset):
    name = dataset[0]
    filename = 'datasets/' + dataset[1]
    if name != 'Netflix':
        with open(filename, 'r') as f:
            lines = f.readlines()

    else:
        num_items = 17770
        users = set()
        items = set()
        for movie in range(1, num_items + 1):
            with open(filename + 'mv_{:07d}.txt'.format(movie), 'r') as f:
                lines = f.readlines()
            for line in lines[1:]:
                user = int(line.split(',')[0])
                users.add(user)
            items.add(movie)
        users = sample(list(users), 50000)
        user_set = set(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        assert num_users == 50000
        assert num_items == len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for movie in range(1, num_items + 1):
            with open(filename + 'mv_{:07d}.txt'.format(movie), 'r') as f:
                lines = f.readlines()
            item = bisect_left(items, movie)
            assert item == movie - 1
            for line in lines[1:]:
                data = line.split(',')
                user, rating = int(data[0]), int(data[1])
                if user not in user_set: continue
                user = bisect_left(users, user)
                year, month, day = map(int, data[2].split('-'))
                ts = year * 1000 + month * 20 + day
                ts = uniform(0, 1)
                user_list[user].append((item, rating, ts))
                item_list[item].append((user, rating, ts))

    if 'MovieLens' in name:
        delimiter = [None, '::']['1M' in name]
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split(delimiter)[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split(delimiter)
            user, item, rating = int(data[0]), int(data[1]), float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = int(data[3])
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'Anime' or name == "SongsDataset":
        users = set()
        items = set()
        for line in lines:
            line = line.split(',')
            user, item = int(line[0]), line[1]
            if int(line[2]) == -1:
                continue
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split(',')
            user, item, rating = int(data[0]), data[1], int(data[2])
            if rating == -1:
                continue
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'BookCrossing':
        users = set()
        items = set()
        for line in lines:
            line = line.split(';')[:2]
            user, item = int(line[0]), line[1]
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split(';')
            user, item, rating = int(data[0]), data[1], float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'MovieTweetings':
        users = set()
        items = set()
        for line in lines:
            line = line.split('::')[:2]
            user, item = int(line[0]), line[1]
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split('::')
            user, item, rating = int(data[0]), data[1], float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = int(data[3])
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if 'Yahoo' in name:
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split()[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split()
            user, item, rating = int(data[0]), int(data[1]), int(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'CiaoDVD':
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split(',')[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split(',')
            user, item, rating = int(data[0]), int(data[1]), float(data[4])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            year, month, day = map(int, data[5].split('-'))
            ts = year * 1000 + month * 20 + day
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'Epinions':
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split()[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split()
            user, item, rating = int(data[0]), int(data[1]), float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'Amazon QoS':
        num_users = len(lines)
        num_items = len(lines[0].split())
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for i, line in enumerate(lines):
            row = line.split()
            assert num_items == len(row)
            for j, rating in enumerate(row):
                rating = float(rating)
                ts = uniform(0, 1)
                user_list[i].append((j, rating, ts))
                item_list[j].append((i, rating, ts))

    if name == 'Jester':
        num_users = len(lines)
        num_items = len(lines[0].split(',')[1:])
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for i, line in enumerate(lines):
            row = line.split(',')[1:]
            assert num_items == len(row)
            for j, rating in enumerate(row):
                rating = float(rating)
                if rating != 99.0:
                    ts = uniform(0, 1)
                    user_list[i].append((j, rating, ts))
                    item_list[j].append((i, rating, ts))

    if name == 'FilmTrust': 
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split()[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split()
            user, item, rating = int(data[0]), int(data[1]), float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if name == 'Flixter':
        users = set()
        items = set()
        for line in lines:
            user, item = map(int, line.split()[:2])
            users.add(user)
            items.add(item)
        users = list(users)
        items = list(items)
        users.sort()
        items.sort()
        num_users = len(users)
        num_items = len(items)
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for line in lines:
            data = line.split()
            user, item, rating = int(data[0]), int(data[1]), float(data[2])
            user = bisect_left(users, user)
            item = bisect_left(items, item)
            ts = uniform(0, 1)
            user_list[user].append((item, rating, ts))
            item_list[item].append((user, rating, ts))

    if 'WS-DREAM' in name:
        # Turkey's method outliers
        if 'TP' in name:
            TRESHOLD = 82.949
        if 'RT' in name:
            TRESHOLD = 1.447
        num_users = len(lines)
        num_items = len(lines[0].split())
        user_list = [[] for i in range(num_users)]
        item_list = [[] for j in range(num_items)]
        for i, line in enumerate(lines):
            row = line.split()
            assert num_items == len(row)
            for j, rating in enumerate(row):
                rating = float(rating)
                if rating != -1 and rating < TRESHOLD:
                    ts = uniform(0, 1)
                    user_list[i].append((j, rating, ts))
                    item_list[j].append((i, rating, ts))

    all_ts = [ts for sublist in user_list for j, rating, ts in sublist]
    all_ts_check = [ts for sublist in item_list for i, rating, ts in sublist]
    mid_ts = np.percentile(all_ts, sys.argv[1])
    mid_ts_check = np.percentile(all_ts_check, sys.argv[1])
    n = len(all_ts)
    assert n == len(all_ts_check)
    assert abs(sum(all_ts) - sum(all_ts_check)) < 1e-6
    assert mid_ts == mid_ts_check

    def f(l):
        d = dict(l)
        l = sorted(d.items(), key=lambda x: x[1])
        return (np.array([i for i, r in l]), np.array([r for i, r in l]))

    def g(l):
        d = dict(l)
        return [(k, d[k]) for k in sorted(d)]

    train_user_list = [
            f([(j, rating) for j, rating, ts in user_list[i] if ts <= mid_ts])
            for i in range(num_users)]
    test_user_list = [
            g([(j, rating) for j, rating, ts in user_list[i] if ts > mid_ts])
            for i in range(num_users)]

    train_item_list = [
            f([(i, rating) for i, rating, ts in item_list[j] if ts <= mid_ts])
            for j in range(num_items)]
    test_item_list = [
            g([(i, rating) for i, rating, ts in item_list[j] if ts > mid_ts])
            for j in range(num_items)]

    with open('pickle/' + name, 'wb', pickle.HIGHEST_PROTOCOL) as f:
        pickle.dump(train_user_list, f)
        pickle.dump(train_item_list, f)
        pickle.dump(test_user_list, f)
        pickle.dump(test_item_list, f)
    print(name, num_users, num_items)
    print(n, num_users * num_items, n / (num_users * num_items))
    print(get_diff(train_user_list))
    print()


all_sets = [
    ('MovieLens 100K', 'ml-100k/u.data'),
    ('Netflix', 'netflix/training_set/'),
    ('CiaoDVD', 'ciao/movie-ratings.txt'),
    ('Amazon QoS', 'amazon/rel1.txt'),
    ('Epinions', 'epinions/ratings_data.txt'),
    ('FilmTrust', 'filmtrust/ratings.txt'),
    ('Flixter', 'flixter/ratings.txt'),
    ('Jester', 'jester/jesterfinal151cols.csv'),
    ('MovieLens 1M', 'ml-1m/ratings.dat'),
    ('BookCrossing', 'bookcrossing/BX-Book-Ratings.csv'),
    ('MovieTweetings', 'MovieTweetings/ratings.dat'),
    ('Anime', 'Anime/rating.csv'),
    ('Yahoo4', 'y4/ratings.txt'),
    ]
with Pool(processes=4) as p:
    p.map(parse, all_sets)
